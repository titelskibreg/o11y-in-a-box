# o11y-in-a-box

[![Build Status](https://ci.certomodo.io/api/badges/certomodo.io/o11y-in-a-box/status.svg)](https://ci.certomodo.io/certomodo.io/o11y-in-a-box)

Ansible playbook that provisions Grafana (dashboarding/alerting), Prometheus (monitoring/time series), and Loki (log storage and querying) on a single Ubuntu 22.04 host.

Reasons to use this:
 * You manage small-scale infrastructure and therefore have modest hardware requirements for your monitoring stack.
 * You want to set up log aggregation and querying but don't have the time or resources to manage ELK or Greylog.
 * Your infrastructure isn't 'cloud-native' (eg: bare-metal, VMs, network gear, etc).
 * You wish to quickly POC/demo these tools on a local VM.

## Server Setup

### Prerequisite: Install Ansible
Ansible is a configuration management (CM) tool. Install it on your local machine or jump host by [following the instructions provided](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) for your OS.

### Prerequisite: Launch a Ubuntu 22.04 Host

Install Ubuntu 22.04 on a VM or bare-metal server to be used to run the monitoring stack. Make sure:
 * that sshd is installed and enabled;
 * that the server has sufficient CPU and storage for the number of systems monitored and the level of data retention required.

Recommended minimum hardware:

 * 2 CPU cores
 * 4 GB of memory
 * 20 GB of free disk space

**Take note of the IP address of the server** as we will be using it often for the rest of this procedure.

### Clone This Repository

`git clone https://gitlab.com/certomodo.io/o11y-in-a-box.git`


### Create an Inventory File
From within the repo, create a file called `hosts` to supply the IP address of the launched Ubuntu 22.04 server.

```
all:
  children:
    mon:
      hosts:
        grafana:
          ansible_host: IPADDR
```

### Run Ansible

```
ansible-playbook -i hosts mon-standalone.yml
```

* Note: If the server is not configured for SSH key authentication, also use flag `-k` for password auth.
* Note: If you need to supply a password to `sudo` on the server to become root, also use flag `-K`.


### Go To Grafana UI
Grafana will be accessible at http://IPADDR:3000 (Default credentials: admin/admin). The following data sources will be immediately available:

* The monitoring server's time series data (via Prometheus scraping node\_exporter on localhost)
* The monitoring server's syslog (via Loki recieving log events from promtail on localhost).

These data sources will be sufficient for building queries, graphs, and dashboards to familiarize yourself with the tools. To monitor additional systems, see the below section.

## Monitoring Setup
This section describes how to configure your monitoring server to gather metrics from your infrastructure. When making changes to the Ansible playbook or the config file templates, be sure to run `ansible-playbook -i hosts mon-standalone.yml` again to apply them to the server.

### Route Syslog to the Monitoring Server
The server is already configured to receive syslog traffic. Configure each host to forward its logs to IPADDR:1514 (TCP). 

#### Linux
There are multiple ways to do this. 

Simple example configuration for syslog-ng:

```
destination d_loki {
  syslog("IPADDR" transport("tcp") port(1514));
};

log {
  source(s_sys);
  destination(d_loki);
};
```

Example config for rsyslog:

```
*.*  action(type="omfwd" target="IPADDR" port="1514" protocol="tcp"
            action.resumeRetryCount="100"
            queue.type="linkedList" queue.size="10000")
````

#### Windows
* NXLog has a [community edition](https://nxlog.co/downloads/nxlog-ce#nxlog-community-edition).
* Solarwinds has a free [event log forwarder](https://www.solarwinds.com/free-tools/event-log-forwarder-for-windows).



### URL Monitoring
Simple website monitoring can be easily achieved via Prometheus' blackbox\_exporter, which will track HTTP response codes, latencies, etc.

Edit `conf/prometheus.j2` to add additional URLs to the already-configured `urlmon` job.

Note: The blackbox\_exporter supports monitoring of additional network protocols. Consult the [documentation](https://github.com/prometheus/blackbox_exporter/blob/master/CONFIGURATION.md) and [example configs](https://github.com/prometheus/blackbox_exporter/blob/master/example.yml).

### Host Monitoring
Prometheus gathers metrics by 'scraping' data from a target's host agent service called an 'exporter'. The most commonly-used one is the node\_exporter, which exposes resource utilization metrics on Linux systems. There are [many, many more](https://prometheus.io/docs/instrumenting/exporters/) for different operating systems as well as software services such as webservers and databases.

Once those exporters are installed, edit `conf/prometheus.j2`.
* For node\_exporter, simply add additional targets to the `node` job.
* For other exporters, add a new job to the existing `scrape_configs`. ([config reference](https://prometheus.io/docs/prometheus/latest/configuration/configuration/)]


## Credits
Courtesy of Amin Astaneh from [Certo Modo](https://certomodo.io)
